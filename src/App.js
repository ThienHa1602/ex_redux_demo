import logo from "./logo.svg";
import "./App.css";
import DemoReduxMini from "./DemoReduxMini/DemoReduxMini";

function App() {
  return (
    <div className="App">
      <DemoReduxMini />
    </div>
  );
}

export default App;
